#!/bin/bash

STARTINGDIR=$PWD
WORKDIR=$HOME/ldd2
IMG=$WORKDIR/qemu-image.img
IMGDIR=$WORKDIR/mount-point.dir
IMGSZ=4g
LINUXSRC=$WORKDIR/linux

setup_workdir() {

echo "Entering $WORKDIR"
mkdir -p $WORKDIR
cd $WORKDIR

}

exit_workdir() {
echo "Exiting $WORKDIR"
cd $STARTINGDIR
}


install_tools() {

echo "Installing build tools"

sudo apt-get install -y \
	libelf-dev \
	flex \
	bison \
	vim \
	build-essential \
	binutils \
	git \
	debootstrap \
	qemu-system-misc \
	qemu-system-common \
	qemu-system-x86 \
	qemu-system-arm

sudo usermod -a -G kvm $USER

}

create_img() {

cd $WORKDIR
qemu-img create $IMG $IMGSZ
mkfs.ext2 $IMG
mkdir -p $IMGDIR
sudo mount -o loop $IMG $IMGDIR
sudo debootstrap --arch amd64 --include="sudo,vim,openssh-server,binutils,make,flex,bison,build-essential,ipython" stretch $IMGDIR

}

setup_img() {

cd $WORKDIR
cat <<EOF | sudo tee $IMGDIR/setup_qemu.sh
#!/bin/bash
echo "Setup user"
mkdir /home/$USER
adduser --quiet --disabled-password --shell /bin/bash --home /home/$USER --gecos "User" $USER
echo "$USER:password" | chpasswd

usermod -a -G sudo $USER
usermod -a -G adm $USER
usermod -a -G plugdev $USER
usermod -a -G cdrom $USER

echo $'\n\nauto enp0s3\niface enp0s3 inet dhcp\n\nsource-directory /etc/network/interfaces.d\n\n' > /etc/network/interfaces
exit

EOF

sudo chmod a+x $IMGDIR/setup_qemu.sh
sudo chroot $IMGDIR /setup_qemu.sh
sudo rm -f $IMGDIR/setup_qemu.sh
sudo umount $IMGDIR

}

checkout_build_linux() {

git clone --depth=1 git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git $LINUXSRC
cd $LINUXSRC
make mrproper
make x86_64_defconfig
make kvmconfig
make -j$(nproc)
ln -s $LINUXSRC/arch/x86/boot/bzImage $WORKDIR
cd $WORKDIR

}

create_qemu_start_script() {

cat <<EOF > $WORKDIR/start_qemu.sh
#!/bin/bash

sudo qemu-system-x86_64 -m 1024 -kernel $WORKDIR/bzImage \
		-append "root=/dev/sda rw console=ttyS0" \
                -hda $IMG \
                --enable-kvm --nographic \
		-net nic -net user,hostfwd=tcp::5555-:22
EOF

chmod u+x $WORKDIR/start_qemu.sh

}


do_qemu_setup() {

install_tools
setup_workdir
create_img
setup_img
checkout_build_linux
create_qemu_start_script
exit_workdir
echo "Completed qemu setup!"
echo "Run $WORKDIR/start_qemu.sh to start your linux emulator"
echo "Connect to it via ssh by running: "
echo "$ ssh -p 5555 localhost"

}


do_qemu_setup
	
